﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using Kontur.GameStats.Server.Utilities.EntitiesUtilities;
using Kontur.GameStats.Server.Utilities.Json;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class PutMatchInfoHandlerTests
    {
        private IRecordsStorage<ServerDataRecord> serverRecordsStorage;
        private IRecordsStorage<MatchDataRecord> matchRecordsStorage;
        private IRecordsStorage<PlayerDataRecord> playerRecordsStorage;
        private IRecordsUpdater<ServerDataRecord> serverRecordsUpdater;
        private IRecordsUpdater<PlayerDataRecord> playerRecordsUpdater;
        private HttpServerContext context;
        private PutMatchInfoHandler handler;
        private IJsonHelper jsonHelper;
        private string endpoint;
        private string timestamp;

        [OneTimeSetUp]
        public void Setup()
        {
            serverRecordsStorage = Substitute.For<IRecordsStorage<ServerDataRecord>>();
            matchRecordsStorage = Substitute.For<IRecordsStorage<MatchDataRecord>>();
            playerRecordsStorage = Substitute.For<IRecordsStorage<PlayerDataRecord>>();
            serverRecordsUpdater = Substitute.For<IRecordsUpdater<ServerDataRecord>>();
            playerRecordsUpdater = Substitute.For<IRecordsUpdater<PlayerDataRecord>>();
            jsonHelper = Substitute.For<IJsonHelper>();
            endpoint = "192.168.0.1-28965";
            context = new HttpServerContext();
            timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddThh:mm:ss");
            handler = new PutMatchInfoHandler(serverRecordsStorage, matchRecordsStorage,
                serverRecordsUpdater, playerRecordsUpdater, playerRecordsStorage, jsonHelper);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnBadRequest_IfEndpointMissing()
        {
            var result = await handler.HandleRequestAsync(context);
            result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnBadRequest_IfTimeStampMissing()
        {
            context.Parameters.Add("endpoint", endpoint);
            serverRecordsStorage.SearchRecords(x => x.Endpoint == endpoint).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>
                .CreateSuccessful(new [] { new ServerDataRecord()}));

            var result = await handler.HandleRequestAsync(context);
            result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnBadRequest_IfServerNotFound()
        {
            serverRecordsStorage.SearchRecords(x => x.Endpoint == endpoint).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>
                .CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);
            result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}
