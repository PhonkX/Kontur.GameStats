﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class GetServerInfoHandlerTests
    {
        private IRecordsStorage<ServerDataRecord> storage;
        private GetServerInfoHandler handler;
        private HttpServerContext context;

        [OneTimeSetUp]
        public void Setup()
        {
            storage = Substitute.For<IRecordsStorage<ServerDataRecord>>();
            handler = new GetServerInfoHandler(storage);
            context = new HttpServerContext();
            context.Parameters.Add("endpoint", "127.0.0.1-28965");
        }

        [Test]
        public async Task HandleRequestAsync_Should_ReturnOkAndInfo_IfInfoRecorded()
        {
            var info = new ServerInfo
            {
                Name = "Practice Server DM FFA",
                GameModes = new[] {"DM"}
            };
            var record = new ServerDataRecord("127.0.0.1-28965", info);
           
            storage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>())
                .Returns(OperationResult<IEnumerable<ServerDataRecord>>
                .CreateSuccessful(new [] {record}));
            
            var result = await handler.HandleRequestAsync(context);
            result.StatusCode.Should().Be(HttpStatusCode.OK);
            result.DataTemporaryObject.ShouldBeEquivalentTo(info);
        }

        [Test]
        public async Task HandleRequestAsync_Should_ReturnNotFound_IfInfoNotFound()
        {
            storage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>())
                .Returns(OperationResult<IEnumerable<ServerDataRecord>>
                .CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);
            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
            result.DataTemporaryObject.ShouldBeEquivalentTo(null);
        }
    }
}
