﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Reports;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class GetTopPlayersHandlerTests
    {
        private IRecordsStorage<PlayerDataRecord> playerRecordsStorage;
        private HttpServerContext context;
        private GetTopPlayersHandler handler;
        private List<PlayerDataRecord> topPlayersRecords;
        private int count;

        [OneTimeSetUp]
        public void Setup()
        {
            playerRecordsStorage = Substitute.For<IRecordsStorage<PlayerDataRecord>>();
            context = new HttpServerContext();
            handler = new GetTopPlayersHandler(playerRecordsStorage);
            count = 2;
            topPlayersRecords = new List<PlayerDataRecord>
            {
                new PlayerDataRecord("Bob1337"),
                new PlayerDataRecord("Bot BigBoss"),
                new PlayerDataRecord("Alice1337"),
                new PlayerDataRecord("T0p1-Pl4y3Rrrr"),
                new PlayerDataRecord("Tru0T0p1Pl0y3r")
            };
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnEmptyArray_IfRecordsNotFound()
        {
            playerRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<PlayerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<PlayerDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);
            var dto = (TopPlayersReport[]) result.DataTemporaryObject;

            dto.Length.Should().Be(0);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnDefaultCountOfRecord_IfCountParameterIsMissing()
        {
            playerRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<PlayerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<PlayerDataRecord>>.CreateSuccessful(topPlayersRecords));

            var result = await handler.HandleRequestAsync(context);
            var dto = (TopPlayersReport[]) result.DataTemporaryObject;

            dto.Length.Should().Be(GlobalConstants.DefaultCountForReports);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnEmptyArray_IfCountEqualsToZero()
        {
            context.Parameters.Add("count", "0");
            playerRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<PlayerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<PlayerDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);
            var dto = (TopPlayersReport[]) result.DataTemporaryObject;

            dto.Length.Should().Be(0);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnCountOfRecord_EqualsToCountParameter()
        {
            playerRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<PlayerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<PlayerDataRecord>>.CreateSuccessful(
                    topPlayersRecords.Take(count)
                ));

            var result = await handler.HandleRequestAsync(context);
            var dto = (TopPlayersReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(count);
        }
    }
}
