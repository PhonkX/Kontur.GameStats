﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class GetMatchInfoHandlerTests
    {
        private IRecordsStorage<ServerDataRecord> serverRecordsStorage;
        private IRecordsStorage<MatchDataRecord> matchRecordsStorage;
        private GetMatchInfoHandler handler;
        private HttpServerContext context;
        private string endpoint;
        private string timestamp;
        private string serverName;
        private string mapName;
        private ServerDataRecord serverDataRecord;
        private MatchDataRecord matchDataRecord;

        [OneTimeSetUp]
        public void Setup()
        {
            serverRecordsStorage = Substitute.For<IRecordsStorage<ServerDataRecord>>();
            matchRecordsStorage = Substitute.For<IRecordsStorage<MatchDataRecord>>();
            handler = new GetMatchInfoHandler(serverRecordsStorage, matchRecordsStorage);
            context = new HttpServerContext();
            endpoint = "192.168.0.1-28965";
            context.Parameters.Add("endpoint", endpoint);
            timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddThh:mm:ssz");
            context.Parameters.Add("timestamp", timestamp);
            serverName = "My amazing local server";
            mapName = "mp_backlot";
            serverDataRecord = new ServerDataRecord
            {
                Endpoint = endpoint,
                Name = serverName
            };

            matchDataRecord = new MatchDataRecord
            {
                Server = endpoint,
                TimeStampString = timestamp,
                Map = mapName,
                Scoreboard = "Bob:20:20:1,T0p1Pl4yz0r:1:20:20"
            };
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnOkAndMatchInfo_IfAllRecordsFound()
        {
            serverRecordsStorage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(new [] {serverDataRecord}));
            matchRecordsStorage.SearchRecords(Arg.Any<Func<MatchDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<MatchDataRecord>>.CreateSuccessful(new [] {matchDataRecord}));

            var result = await handler.HandleRequestAsync(context);
            var dto = (MatchInfo)result.DataTemporaryObject;

            result.StatusCode.Should().Be(HttpStatusCode.OK);
            dto.Map.Should().Be(mapName);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnNotFoundAndNullDto_IfServerRecordNotFound()
        {
            serverRecordsStorage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);

            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
            result.DataTemporaryObject.Should().BeNull();
        }

        public async void HandleRequestAsync_ShouldReturnNotFound_IfMatchRecordNotFound()
        {
            serverRecordsStorage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(new [] {serverDataRecord}));
            matchRecordsStorage.SearchRecords(Arg.Any<Func<MatchDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<MatchDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);

            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
            result.DataTemporaryObject.Should().BeNull();
        }
    }
}
