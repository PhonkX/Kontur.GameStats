﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Stats;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class GetServerStatsHandlerTests
    {
        private IRecordsStorage<ServerDataRecord> serverRecordsStorage;
        private HttpServerContext context;
        private GetServerStatsHandler handler;
        private string endpoint;
        private int matchesCount;
        private int population;

        [OneTimeSetUp]
        public void Setup()
        {
            serverRecordsStorage = Substitute.For<IRecordsStorage<ServerDataRecord>>();
            context = new HttpServerContext();
            endpoint = "192.168.1.1-28965";
            context.Parameters.Add("endpoint", endpoint);
            population = 30;
            matchesCount = 1;
            handler = new GetServerStatsHandler(serverRecordsStorage);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnNotFound_IfRecordNotFound()
        {
            serverRecordsStorage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>
                .CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);

            result.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnOkAndStats_IfRecordExists()
        {
            serverRecordsStorage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(
                    new [] { new ServerDataRecord
                {
                    Endpoint = endpoint,
                    TotalMatchesCount = matchesCount,
                    TotalPlayersCount = population
                }}));

            var result = await handler.HandleRequestAsync(context);
            var stats = (ServerStats) result.DataTemporaryObject;

            result.StatusCode.Should().Be(HttpStatusCode.OK);
            stats.AveragePopulation.Should().Be(
                (double) population / (double) matchesCount
                );
        }
    }
}
