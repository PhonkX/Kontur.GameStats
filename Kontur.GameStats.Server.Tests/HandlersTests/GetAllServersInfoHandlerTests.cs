﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class GetAllServersInfoHandlerTests
    {
        private GetAllServersInfoHandler handler;
        private IRecordsStorage<ServerDataRecord> serverRecordsStorage;
        private HttpServerContext context;
        private List<ServerDataRecord> allServersDataRecords;

        [OneTimeSetUp]
        public void Setup()
        {
            serverRecordsStorage = Substitute.For<IRecordsStorage<ServerDataRecord>>();
            context = new HttpServerContext();
            handler = new GetAllServersInfoHandler(serverRecordsStorage);
            allServersDataRecords = new List<ServerDataRecord>
            {
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28965",
                    AvailableGameModes = "DM,TDM"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28966",
                    AvailableGameModes = "DM,TDM"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.2-28965",
                    AvailableGameModes = "DM,TDM"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28966",
                    AvailableGameModes = "DM,TDM"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28967",
                    AvailableGameModes = "DM,TDM"
                },
            };
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnEmptyArray_IfServersDataRecordsNotFound()
        {
            serverRecordsStorage.SearchRecords().Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);

            var dto = (ExtendedServerInfo[])result.DataTemporaryObject;
            dto.Length.Should().Be(0);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnItems_IfRecordsExists()
        {
            serverRecordsStorage.SearchRecords().Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(allServersDataRecords));

            var result = await handler.HandleRequestAsync(context);
            var dto = (ExtendedServerInfo[]) result.DataTemporaryObject;

            dto.Length.Should().Be(allServersDataRecords.Count);
        }
    }
}
