﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using Kontur.GameStats.Server.Utilities.Json;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class PutServerInfoHandlerTests
    {
        private PutServerInfoHandler handler;
        private IRecordsStorage<ServerDataRecord> storage;
        private HttpServerContext context;
        private IJsonHelper jsonHelper;

        [OneTimeSetUp]
        public void Setup()
        {
            storage = Substitute.For<IRecordsStorage<ServerDataRecord>>();
            jsonHelper = Substitute.For<IJsonHelper>();
            handler = new PutServerInfoHandler(storage, jsonHelper);
            context = new HttpServerContext
            {
                Parameters    = new NameValueCollection
                {
                    ["endpoint"] = "192.168.1.1-28965"
                },
            };
            storage.SearchRecords(Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>
                .CreateError(OperationStatusCode.NotFound));
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnBadRequest_IfEndpointMissing()
        {
            var localContext = new HttpServerContext();
            var result = await handler.HandleRequestAsync(localContext);
            result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Test]
        public async Task HandleRequestAsync_Should_ReturnOkResult() 
        {
            var result = await handler.HandleRequestAsync(context);
            result.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Test]
        public async Task HandleRequestAsync_Should_ReturnEmptyBody()
        {
            var result = await handler.HandleRequestAsync(context);
            result.DataTemporaryObject.Should().Be(null);
        }
    }
}
