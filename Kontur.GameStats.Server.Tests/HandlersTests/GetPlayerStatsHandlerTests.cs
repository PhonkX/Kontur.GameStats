﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Stats;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    namespace Kontur.GameStats.Server.Tests.HandlersTests
    {
        [TestFixture]
        public class GetPlayerStatsHandlerTests
        {
            private IRecordsStorage<PlayerDataRecord> playerRecordsStorage;
            private HttpServerContext context;
            private GetPlayerStatsHandler handler;
            private string playerName;
            private int totalMatchesPlayed;

            [OneTimeSetUp]
            public void Setup()
            {
                playerRecordsStorage = Substitute.For<IRecordsStorage<PlayerDataRecord>>();
                context = new HttpServerContext();
                playerName = "Bob1337";
                context.Parameters.Add("name", playerName);
                totalMatchesPlayed = 30;
                handler = new GetPlayerStatsHandler(playerRecordsStorage);
            }

            [Test]
            public async Task HandleRequestAsync_ShouldReturnNotFound_IfRecordNotFound()
            {
                playerRecordsStorage.SearchRecords(Arg.Any<Func<PlayerDataRecord, bool>>()).Returns(
                    OperationResult<IEnumerable<PlayerDataRecord>>.CreateError(OperationStatusCode.NotFound));

                var result = await handler.HandleRequestAsync(context);

                result.StatusCode.Should().Be(HttpStatusCode.NotFound);
            }

            [Test]
            public async Task HandleRequestAsync_ShouldReturnOkAndStats_IfRecordExists()
            {
                playerRecordsStorage.SearchRecords(Arg.Any<Func<PlayerDataRecord, bool>>()).Returns(
                    OperationResult<IEnumerable<PlayerDataRecord>>.CreateSuccessful(new [] {
                        new PlayerDataRecord
                        {
                            TotalMatchesPlayed = totalMatchesPlayed,
                            VisitedServers = "192.168.1.1-28965:1",
                            GameModes = "DM:1"
                        }}));

                var result = await handler.HandleRequestAsync(context);
                var stats = (PlayerStats)result.DataTemporaryObject;

                result.StatusCode.Should().Be(HttpStatusCode.OK);
                stats.TotalMatchesPlayed.Should().Be(totalMatchesPlayed);
            }
        }
    }

}
