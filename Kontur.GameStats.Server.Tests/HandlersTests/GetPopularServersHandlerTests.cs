﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Reports;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class GetPopularServersHandlerTests
    {
        private IRecordsStorage<ServerDataRecord> serverRecordsStorage;
        private HttpServerContext context;
        private GetPopularServersHandler handler;
        private List<ServerDataRecord> popularServersRecords;
        private int count;

        [OneTimeSetUp]
        public void Setup()
        {
            serverRecordsStorage = Substitute.For<IRecordsStorage<ServerDataRecord>>();
            context = new HttpServerContext();
            handler = new GetPopularServersHandler(serverRecordsStorage);
            count = 2;
            popularServersRecords = new List<ServerDataRecord>
            {
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28965"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28966"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.2-28965"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28966"
                },
                new ServerDataRecord
                {
                    Endpoint = "192.168.1.1-28967"
                },
            };
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnEmptyArray_IfRecordsNotFound()
        {
            context.Parameters["count"] = count.ToString();
            serverRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);
            var dto = (PopularServersReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(0);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnDefaultCountOfRecord_IfCountParameterIsMissing()
        {
            if (context.Parameters["count"] != null)
            {
                context.Parameters.Remove("count");
            }
            serverRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(popularServersRecords));

            var result = await handler.HandleRequestAsync(context);
            var dto = (PopularServersReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(GlobalConstants.DefaultCountForReports);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnEmptyArray_IfCountEqualsToZero()
        {
            context.Parameters.Add("count", "0");
            serverRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);
            var dto = (PopularServersReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(0);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnCountOfRecord_EqualsToCountParameter()
        {
            var countString = count.ToString();
            if (context.Parameters["count"] != null)
            {
                context.Parameters["count"] = countString;
            }
            else
            {
                context.Parameters.Add("count", countString);
            }
            serverRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<ServerDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(
                    popularServersRecords.Take(count)
                ));

            var result = await handler.HandleRequestAsync(context);
            var dto = (PopularServersReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(count);
        }
    }
}
