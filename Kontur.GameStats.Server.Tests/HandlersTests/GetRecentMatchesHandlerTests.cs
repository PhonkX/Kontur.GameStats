﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Reports;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.HandlersTests
{
    [TestFixture]
    public class GetRecentMatchesHandlerTests
    {
        private IRecordsStorage<MatchDataRecord> matchRecordsStorage;
        private HttpServerContext context;
        private GetRecentMatchesHandler handler;
        private List<MatchDataRecord> recentMatchesRecords;
        private int count;

        [OneTimeSetUp]
        public void Setup()
        {
            matchRecordsStorage = Substitute.For<IRecordsStorage<MatchDataRecord>>();
            context = new HttpServerContext();
            handler = new GetRecentMatchesHandler(matchRecordsStorage);
            count = 2;
            recentMatchesRecords = new List<MatchDataRecord>
            {
                new MatchDataRecord
                {
                    TimeStamp = DateTime.UtcNow.Ticks,
                    Scoreboard = "Bob:20:20:1,T0p1Pl4yz0r:1:20:20"
                },

                new MatchDataRecord
                {
                    TimeStamp = DateTime.UtcNow.Ticks + 10,
                    Scoreboard = "Bob:20:20:1,T0p1Pl4yz0r:1:20:20"
                },

                new MatchDataRecord
                {
                    TimeStamp = DateTime.UtcNow.Ticks + 20,
                    Scoreboard = "Bob:20:20:1,T0p1Pl4yz0r:1:20:20"
                },

                new MatchDataRecord
                {
                    TimeStamp = DateTime.UtcNow.Ticks + 30,
                    Scoreboard = "Bob:20:20:1,T0p1Pl4yz0r:1:20:20"
                },

                new MatchDataRecord
                {
                    TimeStamp = DateTime.UtcNow.Ticks + 40,
                    Scoreboard = "Bob:20:20:1,T0p1Pl4yz0r:1:20:20"
                }
            };
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnEmptyArray_IfRecordsNotFound()
        {
            context.Parameters["count"] = count.ToString();
            matchRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<MatchDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<MatchDataRecord>>.CreateError(OperationStatusCode.NotFound));

            var result = await handler.HandleRequestAsync(context);
            var dto = (RecentMatchesReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(0);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnDefaultCountOfRecord_IfCountParameterIsMissing()
        {
            if (context.Parameters["count"] != null)
            {
                context.Parameters.Remove("count");
            }
            matchRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<MatchDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<MatchDataRecord>>.CreateSuccessful(recentMatchesRecords));

            var result = await handler.HandleRequestAsync(context);
            var dto = (RecentMatchesReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(GlobalConstants.DefaultCountForReports);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnEmptyArray_IfCountEqualsToZero()
        {
            context.Parameters.Add("count", "0");
            matchRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<MatchDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<MatchDataRecord>>.CreateSuccessful(recentMatchesRecords));

            var result = await handler.HandleRequestAsync(context);
            var dto = (RecentMatchesReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(0);
        }

        [Test]
        public async Task HandleRequestAsync_ShouldReturnCountOfRecord_EqualsToCountParameter()
        {
            context.Parameters["count"] = count.ToString();
            matchRecordsStorage.GetTopNRecords(Arg.Any<int>(), Arg.Any<Func<MatchDataRecord, bool>>()).Returns(
                OperationResult<IEnumerable<MatchDataRecord>>.CreateSuccessful(
                    recentMatchesRecords.Take(count)
                ));

            var result = await handler.HandleRequestAsync(context);
            var dto = (RecentMatchesReport[])result.DataTemporaryObject;

            dto.Length.Should().Be(count);
        }
    }
}
