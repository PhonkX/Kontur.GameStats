﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using Kontur.GameStats.Server.Utilities.EntitiesUtilities;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.EntitiesUtilities
{
    [TestFixture]
    public class ServerRecordsUpdaterTests
    {
        private IRecordsStorage<MatchDataRecord> matchRecordsStorage;
        private ServerRecordsUpdater updater;
        private string endpoint;

        [OneTimeSetUp]
        public void Setup()
        {
            matchRecordsStorage = Substitute.For<IRecordsStorage<MatchDataRecord>>();
            updater = new ServerRecordsUpdater(matchRecordsStorage);
            endpoint = "192.168.1.1-28965";
        }

        [Test]
        public async Task UpdateServerRecord_Should_UpdateRecordCorrectly_CaseFirstMatch()
        {
            var serverDataRecord = new ServerDataRecord
            {
                Endpoint = endpoint,
                Name = "Fr4gM4dn3zz",
                AvailableGameModes ="DM,TDM"
            };
            matchRecordsStorage.SearchRecords(Arg.Any<Func<MatchDataRecord, bool>>())
                .Returns(OperationResult<IEnumerable<MatchDataRecord>>.CreateError(OperationStatusCode.NotFound));
            var timeStampDT = DateTime.UtcNow;

            var matchDataRecord = new MatchDataRecord
            {
                Server = endpoint,
                FragLimit = 20,
                GameMode = "DM",
                Map = "Training Camp",
                Scoreboard = "Bob1337:1:20:20,T0p1Pl4yz0r:20:1:1",
                TimeElapsed = 60,
                TimeLimit = 600,
                TimeStamp = timeStampDT.Ticks,
                TimeStampString = timeStampDT.ToString(CultureInfo.InvariantCulture)
            };

            await updater.UpdateRecord(serverDataRecord, matchDataRecord);
            
            serverDataRecord.AverageMatchesPerDay.Should().Be(1.0);
            serverDataRecord.AveragePopulation.Should().Be(2.0);
            serverDataRecord.LastMatchPlayedTimeStamp.Should().Be(timeStampDT.Ticks);
            serverDataRecord.UpTimeInDays.Should().Be(1);
            serverDataRecord.Maps.Should().Contain("Training Camp");
            serverDataRecord.MaxMatchesPerDayCount.Should().Be(1);
            serverDataRecord.MaxPopulation.Should().Be(2);
            serverDataRecord.PlayedGameModes.Should().Contain("DM");
            serverDataRecord.TotalMatchesCount.Should().Be(1);
            serverDataRecord.TotalPlayersCount.Should().Be(2);
        }

        [Test]
        public async Task UpdateServerRecord_Should_UpdateRecordCorrectly_CaseTwoMatchesInDifferentDays()
        {
            var serverDataRecord = new ServerDataRecord
            {
                Endpoint = endpoint,
                Name = "Fr4gM4dn3zz",
                AvailableGameModes = "DM,TDM"
            };
            matchRecordsStorage.SearchRecords(Arg.Any<Func<MatchDataRecord, bool>>())
                .Returns(OperationResult<IEnumerable<MatchDataRecord>>.CreateError(OperationStatusCode.NotFound));
            var timeStampDT = DateTime.UtcNow;

            var matchDataRecord1 = new MatchDataRecord
            {
                Server = endpoint,
                FragLimit = 20,
                GameMode = "DM",
                Map = "Training Camp",
                Scoreboard = "Bob1337:1:20:20,T0p1Pl4yz0r:20:1:1",
                TimeElapsed = 60,
                TimeLimit = 600,
                TimeStamp = timeStampDT.Ticks,
                TimeStampString = timeStampDT.ToString(CultureInfo.InvariantCulture)
            };

            var matchDataRecord2 = new MatchDataRecord
            {
                Server = "192.168.1.1-28965",
                FragLimit = 20,
                GameMode = "DM",
                Map = "Training Camp",
                Scoreboard = "Bob1337:1:20:20,T0p1Pl4yz0r:20:1:1",
                TimeElapsed = 60,
                TimeLimit = 600,
                TimeStamp = timeStampDT.AddDays(1).Ticks,
                TimeStampString = timeStampDT.AddDays(1).ToString(CultureInfo.InvariantCulture)
            };

            await updater.UpdateRecord(serverDataRecord, matchDataRecord1);
            await updater.UpdateRecord(serverDataRecord, matchDataRecord2);

            serverDataRecord.AverageMatchesPerDay.Should().Be(1.0);
            serverDataRecord.AveragePopulation.Should().Be(2.0);
            serverDataRecord.LastMatchPlayedTimeStamp.Should().Be(timeStampDT.AddDays(1).Ticks);
            serverDataRecord.UpTimeInDays.Should().Be(2);
            serverDataRecord.Maps.Should().Contain("Training Camp");
            serverDataRecord.MaxMatchesPerDayCount.Should().Be(1);
            serverDataRecord.MaxPopulation.Should().Be(2);
            serverDataRecord.PlayedGameModes.Should().Contain("DM");
            serverDataRecord.TotalMatchesCount.Should().Be(2);
            serverDataRecord.TotalPlayersCount.Should().Be(4);
        }
    }
}
