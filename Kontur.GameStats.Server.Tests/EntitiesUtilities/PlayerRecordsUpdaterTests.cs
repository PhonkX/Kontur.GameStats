﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using FluentAssertions;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using Kontur.GameStats.Server.Utilities.EntitiesUtilities;
using NSubstitute;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests.EntitiesUtilities
{
    [TestFixture]
    public class PlayerRecordsUpdaterTests
    {
        private PlayerRecordsUpdater playerRecordsUpdater;
        private IRecordsStorage<MatchDataRecord> matchRecordsStorage;
        private string playerName;

        [OneTimeSetUp]
        public void Setup()
        {
            matchRecordsStorage = Substitute.For<IRecordsStorage<MatchDataRecord>>();
            playerRecordsUpdater = new PlayerRecordsUpdater(matchRecordsStorage);
            playerName = "Bob1337";
        }

        [Test]
        public async Task UpdatePlayerRecord_ShouldUpdateRecordCorrectly_CaseFirstMatch()
        {
            matchRecordsStorage.SearchRecords(Arg.Any<Func<MatchDataRecord, bool>>())
                .Returns(OperationResult<IEnumerable<MatchDataRecord>>.CreateError(OperationStatusCode.NotFound));
            var timeStampDT = DateTime.UtcNow;

            var matchDataRecord = new MatchDataRecord
            {
                Server = "192.168.1.1-28965",
                FragLimit = 20,
                GameMode = "DM",
                Map = "Training Camp",
                Scoreboard = $"{playerName}:20:20:1,T0p1Pl4yz0r:1:20:20",
                TimeElapsed = 60,
                TimeLimit = 600,
                TimeStamp = timeStampDT.Ticks,
                TimeStampString = timeStampDT.ToString(CultureInfo.InvariantCulture)
            };

            var playerRecord = new PlayerDataRecord(playerName);

            await playerRecordsUpdater.UpdateRecord(playerRecord, matchDataRecord);

            playerRecord.AverageScoreboardPercent.Should().Be(100);
            playerRecord.DaysPlayed.Should().Be(1);
            playerRecord.GameModes.Should().Contain("DM");
            playerRecord.KillToDeathRatio.Should().Be(20.0);
            playerRecord.LastMatchPlayed.Should().Be(timeStampDT.Ticks);
            playerRecord.TotalDeaths.Should().Be(1);
            playerRecord.TotalKills.Should().Be(20);
            playerRecord.TotalMatchesPlayed.Should().Be(1);
            playerRecord.MaximumMatchesPerDay.Should().Be(1);
            playerRecord.VisitedServers.Should().Contain("192.168.1.1-28965");
            playerRecord.TotalMatchesWon.Should().Be(1);
        }
    }
}
