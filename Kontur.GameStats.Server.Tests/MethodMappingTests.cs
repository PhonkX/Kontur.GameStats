﻿using FluentAssertions;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.Mapping;
using NUnit.Framework;

namespace Kontur.GameStats.Server.Tests
{
    [TestFixture]
    public class MethodMappingTests
    {
        private string prefix;
        private MethodsMapping mapping;

        [OneTimeSetUp]
        public void Setup()
        {
            prefix = "http://localhost:8080";
            mapping = new MethodsMapping(prefix);
        }

        [Test]
        public void Mapping_Should_CorrectlyMapPutMethods_AndParseParameters()
        {
            var url = prefix + "/servers/192.168.1.1-28965/info";
            var result = mapping.GetTemplateByUrl("PUT", url);
            var handler = mapping.GetHandlerByTemplate(result.Template, "PUT");
            var parameters = result.Parameters;

            handler.ShouldBeEquivalentTo(typeof(PutServerInfoHandler));
            parameters["endpoint"].Should().Be("192.168.1.1-28965");
        }

        [Test]
        public void Mapping_Should_CorrectlyMapGetMethods_AndParseParameters()
        {
            var url = prefix + "/servers/192.168.1.1-28965/info";
            var result = mapping.GetTemplateByUrl("GET", url);
            var handler = mapping.GetHandlerByTemplate(result.Template, "GET");
            var parameters = result.Parameters;

            handler.ShouldBeEquivalentTo(typeof(GetServerInfoHandler));
            parameters["endpoint"].Should().Be("192.168.1.1-28965");
        }

        [Test]
        public void Mapping_Should_CorrectlyMapGetMethods_WithoutParameters()
        {
            var url = prefix + "/reports/best-players";
            var result = mapping.GetTemplateByUrl("GET", url);
            var handler = mapping.GetHandlerByTemplate(result.Template, "GET");
            var parameters = result.Parameters;

            handler.ShouldBeEquivalentTo(typeof(GetTopPlayersHandler));
            parameters["count"].Should().Be(null);
        }
    }
}
