﻿using System;
using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace Kontur.GameStats.Logger.Tests
{
    [TestFixture]
    public class LoggerTests
    {
        private ServerLogger logger;

        [OneTimeSetUp]
        public void Setup()
        {
            logger = new ServerLogger();
        }

        [Test]
        public void Logger_Should_LogCorrectly()
        {
            logger.Log.Info("Hello, world!");
            var writtenInformation = File.ReadAllText(Environment.CurrentDirectory + @"\Logs\activity.log");
            writtenInformation.Should().Contain("Hello, world!");
        }
    }
}
