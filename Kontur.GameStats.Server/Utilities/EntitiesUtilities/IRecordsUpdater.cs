﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel.DataRecords;

namespace Kontur.GameStats.Server.Utilities.EntitiesUtilities
{
    public interface IRecordsUpdater<T>
    {
        Task UpdateRecord(T record, MatchDataRecord matchDataRecord);
    }
}
