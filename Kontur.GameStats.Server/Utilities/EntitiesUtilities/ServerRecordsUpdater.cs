﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Utilities;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Utilities.EntitiesUtilities
{
    public class ServerRecordsUpdater : IRecordsUpdater<ServerDataRecord>
    {
        private readonly IRecordsStorage<MatchDataRecord> matchInfoStorage;

        public ServerRecordsUpdater(IRecordsStorage<MatchDataRecord> matchInfoStorage)
        {
            this.matchInfoStorage = matchInfoStorage;
        }

        public async Task UpdateRecord(ServerDataRecord serverDataRecord, MatchDataRecord matchDataRecord)
        {
            serverDataRecord.TotalMatchesCount++;
            serverDataRecord.LastMatchPlayedTimeStamp = matchDataRecord.TimeStamp; 

            UpdateGameModeInfo(serverDataRecord, matchDataRecord.GameMode);
            UpdateMapInfo(serverDataRecord, matchDataRecord.Map);

            var matchEndTime =
                new DateTime(matchDataRecord.TimeStamp).AddSeconds((int) Math.Ceiling(matchDataRecord.TimeElapsed));

            var searchSameDayMatchesResult = await matchInfoStorage.SearchRecords(x => 
                      x.Server == serverDataRecord.Endpoint
                   && new DateTime(x.TimeStamp).Year == matchEndTime.Year
                   && new DateTime(x.TimeStamp).DayOfYear == matchEndTime.DayOfYear);
            var sameDayMatchesCount = searchSameDayMatchesResult.StatusCode == OperationStatusCode.Success
                ? searchSameDayMatchesResult.Result.Count()
                : 0;

            UpdateUpTimeInfo(serverDataRecord, sameDayMatchesCount);
            UpdateMatchesProperties(serverDataRecord, sameDayMatchesCount);  
            UpdatePopulationProperties(serverDataRecord, matchDataRecord);
        }

        private void UpdateGameModeInfo(ServerDataRecord serverDataRecord, string gameMode)
        {
            var playedGameModes = EntityStringDictionaryConvert.ToDictionary(serverDataRecord.PlayedGameModes);
            if (playedGameModes.ContainsKey(gameMode))
            {
                playedGameModes[gameMode]++;
            }
            else
            {
                playedGameModes[gameMode] = 1;
            }

            serverDataRecord.PlayedGameModes = EntityStringDictionaryConvert.ToEntityString(playedGameModes);
        }

        private void UpdateMapInfo(ServerDataRecord serverDataRecord, string map)
        {
            var maps = EntityStringDictionaryConvert.ToDictionary(serverDataRecord.Maps);

            if (maps.ContainsKey(map))
            {
                maps[map]++;
            }
            else
            {
                maps[map] = 1;
            }

            serverDataRecord.Maps = EntityStringDictionaryConvert.ToEntityString(maps);
        }

        private void UpdateMatchesProperties(ServerDataRecord serverDataRecord, int sameDayMatchesCount)
        {
            if (sameDayMatchesCount + 1 > serverDataRecord.MaxMatchesPerDayCount)
            {
                serverDataRecord.MaxMatchesPerDayCount = sameDayMatchesCount + 1;
            }

            serverDataRecord.AverageMatchesPerDay = serverDataRecord.UpTimeInDays != 0
                ? Math.Round((double)serverDataRecord.TotalMatchesCount / (double) serverDataRecord.UpTimeInDays, 6)
                : 0;
        }

        private void UpdateUpTimeInfo(ServerDataRecord serverDataRecord, int lastDayMatchesCount)
        {
            if (lastDayMatchesCount == 0)
            {
                serverDataRecord.UpTimeInDays++;
            }
        }

        private void UpdatePopulationProperties(ServerDataRecord serverDataRecord, MatchDataRecord matchDataRecord)
        {
            var playersList = ScoreboardStringConvertor.ToScoreboard(matchDataRecord.Scoreboard)
                .Select(x => x.Name)
                .ToArray();

            serverDataRecord.TotalPlayersCount += playersList.Length;
            
            if (serverDataRecord.MaxPopulation < playersList.Length)
            {
                serverDataRecord.MaxPopulation = playersList.Length;
            }

            serverDataRecord.AveragePopulation = Math.Round(
                (double) serverDataRecord.TotalPlayersCount / (double) serverDataRecord.TotalMatchesCount, 6);
        }
    }
}
