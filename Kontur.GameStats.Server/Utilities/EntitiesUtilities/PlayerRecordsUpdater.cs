﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Utilities;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Utilities.EntitiesUtilities
{
    public class PlayerRecordsUpdater : IRecordsUpdater<PlayerDataRecord>
    {
        private readonly IRecordsStorage<MatchDataRecord> matchInfoStorage;

        public PlayerRecordsUpdater(IRecordsStorage<MatchDataRecord> matchInfoStorage)
        {
            this.matchInfoStorage = matchInfoStorage;
        }

        public async Task UpdateRecord(PlayerDataRecord playerDataRecord, MatchDataRecord matchDataRecord)
        {
            playerDataRecord.TotalMatchesPlayed++;

            UpdateServersInfo(playerDataRecord, matchDataRecord.Server);

            var scoreboard = ScoreboardStringConvertor.ToScoreboard(matchDataRecord.Scoreboard);

            var playerInScoreboard = scoreboard
                .First(x => x.Name.Equals(playerDataRecord.Player, StringComparison.InvariantCultureIgnoreCase));
            playerDataRecord.TotalKills += playerInScoreboard.Kills;
            playerDataRecord.TotalDeaths += playerInScoreboard.Deaths;
            playerDataRecord.KillToDeathRatio = playerDataRecord.TotalDeaths != 0
                ? Math.Round((double) playerDataRecord.TotalKills / (double)playerDataRecord.TotalDeaths, 6)
                : playerDataRecord.TotalKills;
            UpdateGameModesInPlayerStatsInfo(playerDataRecord, matchDataRecord.GameMode);

            var searchSameDayMatchesResult = await matchInfoStorage.SearchRecords(
                x => x.Server == matchDataRecord.Server
             && ScoreboardStringConvertor.ToScoreboard(x.Scoreboard)
                    .Select(y => y.Name)
                    .Contains(playerDataRecord.Player));

            var sameDayMatchesCount = searchSameDayMatchesResult.StatusCode == OperationStatusCode.Success
                ? searchSameDayMatchesResult.Result.Count()
                : 0;

            if (sameDayMatchesCount == 0)
            {
                playerDataRecord.DaysPlayed++;
            }

            UpdateMatchesInfo(playerDataRecord, matchDataRecord, sameDayMatchesCount);

            UpdateAverageScoreboardPercentAndVictoriesCount(playerDataRecord, matchDataRecord); 
        }

        private void UpdateServersInfo(PlayerDataRecord playerDataRecord, string server)
        {
            var visitedServers = EntityStringDictionaryConvert.ToDictionary(playerDataRecord.VisitedServers);

            if (visitedServers.ContainsKey(server))
            {
                visitedServers[server]++;
            }
            else
            {
                visitedServers[server] = 1;
            }

            playerDataRecord.VisitedServers = EntityStringDictionaryConvert.ToEntityString(visitedServers);
        }

        private void UpdateGameModesInPlayerStatsInfo(PlayerDataRecord playerDataRecord, string gameMode)
        {
            var gameModes = EntityStringDictionaryConvert.ToDictionary(playerDataRecord.GameModes);

            if (gameModes.ContainsKey(gameMode))
            {
                gameModes[gameMode]++;
            }
            else
            {
                gameModes[gameMode] = 1;
            }

            playerDataRecord.GameModes = EntityStringDictionaryConvert.ToEntityString(gameModes);
        }

        private void UpdateMatchesInfo(PlayerDataRecord playerDataRecord, MatchDataRecord matchDataRecord, int sameDayMatchesCount)
        {
            if (sameDayMatchesCount + 1 > playerDataRecord.MaximumMatchesPerDay)
            {
                playerDataRecord.MaximumMatchesPerDay = sameDayMatchesCount + 1;
            }

            if (playerDataRecord.LastMatchPlayed < matchDataRecord.TimeStamp)
            {
                playerDataRecord.LastMatchPlayed = matchDataRecord.TimeStamp;
                playerDataRecord.LastMatchPlayedTimestamp = matchDataRecord.TimeStampString;
            }
        }

        private void UpdateAverageScoreboardPercentAndVictoriesCount(PlayerDataRecord playerDataRecord,
             MatchDataRecord matchDataRecord)
        {
            var playersList = ScoreboardStringConvertor.ToScoreboard(matchDataRecord.Scoreboard)
                .Select(x => x.Name)
                .ToArray();

            int scoreboardPosition = 0;
            while (!ScoreboardStringConvertor.ToScoreboard(matchDataRecord.Scoreboard)[scoreboardPosition++].Name
                .Equals(playerDataRecord.Player, StringComparison.InvariantCultureIgnoreCase)) ;
            var playersBelowCount = playersList.Length - scoreboardPosition;
            var scoreboardPercent = Math.Round((double) playersBelowCount / (double) (playersList.Length - 1) * 100, 6);
            playerDataRecord.AverageScoreboardPercent =
                Math.Round((playerDataRecord.AverageScoreboardPercent * (playerDataRecord.TotalMatchesPlayed - 1) + scoreboardPercent)
                / (playerDataRecord.TotalMatchesPlayed), 6);

            if (Math.Abs(scoreboardPercent) - 100 < 0.00001)
            {
                playerDataRecord.TotalMatchesWon++;
            }
        }
    }
}
