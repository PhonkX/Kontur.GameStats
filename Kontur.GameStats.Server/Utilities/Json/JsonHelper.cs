﻿using System.IO;
using Newtonsoft.Json;

namespace Kontur.GameStats.Server.Utilities.Json
{
    public class JsonHelper : IJsonHelper
    {
        public object DeserializeObject(string jsonString)
        {
            return JsonConvert.DeserializeObject(jsonString);
        }

        public string SerializeObject(object objectToSerialize)
        {
            return JsonConvert.SerializeObject(objectToSerialize);
        }

        public string ReadJsonString(Stream stream)
        {
            var streamReader = new StreamReader(stream);
            var jsonString = streamReader.ReadToEnd();
            return jsonString;
        }

        public T DeserializeObject<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }
    }
}