﻿using System.IO;

namespace Kontur.GameStats.Server.Utilities.Json
{
    public interface IJsonHelper
    {
        object DeserializeObject(string jsonString);
        string SerializeObject(object objectToSerialize);
        string ReadJsonString(Stream stream);
        T DeserializeObject<T>(string jsonString);
    }
}
