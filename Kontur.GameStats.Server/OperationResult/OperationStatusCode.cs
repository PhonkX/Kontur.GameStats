﻿namespace Kontur.GameStats.Server.OperationResult
{
    public enum OperationStatusCode
    {
        Success = 200,
        NotFound = 404,
        UnknownError = 500
    }
}
