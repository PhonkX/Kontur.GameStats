﻿namespace Kontur.GameStats.Server.OperationResult
{
    public class OperationResult<T>
    {
        public T Result { get; }
        public OperationStatusCode StatusCode { get; }

        private OperationResult(T result, OperationStatusCode code)
        {
            Result = result;
            StatusCode = code;
        }

        private OperationResult(OperationStatusCode code)
        {
            StatusCode = code;
        }

        public static OperationResult<T> CreateSuccessful(T result)
        {
            return new OperationResult<T>(result, OperationStatusCode.Success);
        }

        public static OperationResult<T> CreateError(OperationStatusCode code)
        {
            return new OperationResult<T>(code);
        }
    }
}
