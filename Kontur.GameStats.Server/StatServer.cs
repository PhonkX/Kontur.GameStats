﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.Logger;
using Kontur.GameStats.Server.Handlers;
using Kontur.GameStats.Server.Mapping;
using Kontur.GameStats.Server.Utilities.Json;
using SimpleInjector;

namespace Kontur.GameStats.Server
{
    internal class StatServer : IDisposable
    {
        private IJsonHelper jsonHelper;
        private MethodsMapping methodsMapping;
        private Container container;
        private IServerLogger logger;

        public StatServer(Container container)
        {
            listener = new HttpListener();
            logger = container.GetInstance<IServerLogger>();
            jsonHelper = container.GetInstance<IJsonHelper>();
            this.container = container;
        }

        public void Start(string prefix)
        {
            methodsMapping = new MethodsMapping(prefix);
            lock (listener)
            {
                if (!isRunning)
                {
                    listener.Prefixes.Clear();
                    listener.Prefixes.Add(prefix);
                    listener.Start();

                    listenerThread = new Thread(Listen)
                    {
                        IsBackground = true,
                        Priority = ThreadPriority.Highest
                    };
                    listenerThread.Start();

                    isRunning = true;
                }
            }
        }

        public void Stop()
        {
            lock (listener)
            {
                if (!isRunning)
                    return;

                listener.Stop();

                listenerThread.Abort();
                listenerThread.Join();

                isRunning = false;
            }
        }

        public void Dispose()
        {
            if (disposed)
                return;

            disposed = true;

            Stop();

            listener.Close();
        }

        private void Listen()
        {
            while (true)
            {
                if (listener.IsListening)
                {
                    var context = listener.GetContext();
                    Task.Run(() => HandleContextAsync(context));
                }
                else Thread.Sleep(0);
            }
        }

        private async Task HandleContextAsync(HttpListenerContext listenerContext)
        {
            try
            {
                logger.Log.Info($"Incoming request from {listenerContext.Request.RemoteEndPoint.Address}");

                var templateAndParameters = methodsMapping.GetTemplateByUrl(
                    listenerContext.Request.HttpMethod,
                    listenerContext.Request.Url.ToString());

                var httpServerContext = new HttpServerContext
                {
                    Parameters = templateAndParameters.Parameters,
                    Request = listenerContext.Request
                };

                var handlerType = methodsMapping.GetHandlerByTemplate(templateAndParameters.Template,
                    listenerContext.Request.HttpMethod);
                var handler = (IHandler) container.GetInstance(handlerType);

                if (handler == null)
                {
                    listenerContext.Response.StatusCode = (int) HttpStatusCode.BadRequest;

                }
                else
                {
                    var result = await handler.HandleRequestAsync(httpServerContext);

                    listenerContext.Response.StatusCode = (int) result.StatusCode;
                    if (result.DataTemporaryObject != null)
                    {
                        using (var writer = new StreamWriter(listenerContext.Response.OutputStream))
                        {
                            writer.WriteLine(jsonHelper.SerializeObject(result.DataTemporaryObject));
                        }
                    }
                }
            }
            catch (Exception error)
            {
                logger.Log.Error(error.GetType() + " " + error.Message);
                listenerContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
            }
            finally
            {
                listenerContext.Response.Close();
            }
        }

        private readonly HttpListener listener;

        private Thread listenerThread;
        private bool disposed;
        private volatile bool isRunning;
    }
}