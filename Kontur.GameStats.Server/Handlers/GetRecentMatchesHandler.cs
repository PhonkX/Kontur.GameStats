﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Reports;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
    public class GetRecentMatchesHandler: IHandler
    {
        private readonly IRecordsStorage<MatchDataRecord> matchRecordsStorage;

        public GetRecentMatchesHandler(IRecordsStorage<MatchDataRecord> matchRecordsStorage)
        {
            this.matchRecordsStorage = matchRecordsStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            int count = GlobalConstants.DefaultCountForReports;
            string countParameter = httpServerContext.Parameters["count"];
            if (countParameter != null)
            {
                count = Math.Min(int.Parse(countParameter), GlobalConstants.MaxCountForReports);
            }

            if (count == 0)
            {
                return new HandleResult(HttpStatusCode.OK, new RecentMatchesReport[] {});
            }

            var getRecentMatchesResult = await matchRecordsStorage.GetTopNRecords(count);
            if (getRecentMatchesResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.OK, new RecentMatchesReport[] {});
            }

            return new HandleResult(
                HttpStatusCode.OK,
                getRecentMatchesResult.Result
                .Select(x => new RecentMatchesReport(x))
                .ToArray());
        }
    }
}
