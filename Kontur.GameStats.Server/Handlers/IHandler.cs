﻿using System.Threading.Tasks;
using Kontur.GameStats.DataModel;

namespace Kontur.GameStats.Server.Handlers
{
    public interface IHandler
    {
        Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext);
    }
}
