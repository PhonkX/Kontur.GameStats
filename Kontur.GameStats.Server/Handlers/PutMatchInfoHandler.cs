﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using Kontur.GameStats.Server.Utilities.EntitiesUtilities;
using Kontur.GameStats.Server.Utilities.Json;

namespace Kontur.GameStats.Server.Handlers
{
    public class PutMatchInfoHandler: IHandler
    {
        private readonly IRecordsStorage<ServerDataRecord> serverInfoStorage;
        private readonly IRecordsStorage<MatchDataRecord> matchInfoStorage;
        private readonly IRecordsUpdater<ServerDataRecord> serverRecordsUpdater;
        private readonly IRecordsUpdater<PlayerDataRecord> playerRecordsUpdater;
        private readonly IRecordsStorage<PlayerDataRecord> playerRecordsStorage;
        private readonly IJsonHelper jsonHelper;

        public PutMatchInfoHandler(IRecordsStorage<ServerDataRecord> serverInfoStorage,
            IRecordsStorage<MatchDataRecord> matchInfoStorage,
            IRecordsUpdater<ServerDataRecord> serverRecordsUpdater,
            IRecordsUpdater<PlayerDataRecord> playerRecordsUpdater,
            IRecordsStorage<PlayerDataRecord> playerRecordsStorage
            , IJsonHelper jsonHelper)
        {
            this.serverInfoStorage = serverInfoStorage;
            this.serverRecordsUpdater = serverRecordsUpdater;
            this.playerRecordsUpdater = playerRecordsUpdater;
            this.playerRecordsStorage = playerRecordsStorage;
            this.jsonHelper = jsonHelper;
            this.matchInfoStorage = matchInfoStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            var endpoint = httpServerContext.Parameters["endpoint"];
            if (String.IsNullOrWhiteSpace(endpoint))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }

            var timestamp = httpServerContext.Parameters["timestamp"];
            if (String.IsNullOrWhiteSpace(timestamp))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }

            var getServerRecordResult = await serverInfoStorage.SearchRecords(x => x.Endpoint == endpoint);
            if (getServerRecordResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }
            
            var jsonString = jsonHelper.ReadJsonString(httpServerContext.Request.InputStream);
            var matchInfo = jsonHelper.DeserializeObject<MatchInfo>(jsonString);

            var matchDataRecord = new MatchDataRecord(
                getServerRecordResult.Result.First().Endpoint,
                timestamp,
                matchInfo);
            
            await serverRecordsUpdater.UpdateRecord(getServerRecordResult.Result.First(), matchDataRecord);
            var updateServerRecordResult = await serverInfoStorage.WriteRecord(getServerRecordResult.Result.First());
            var playersList = matchInfo.Scoreboard
                .Select(x => x.Name);
            foreach (var playerName in playersList)
            {
                var getPlayerResult = await playerRecordsStorage.SearchRecords(x => x.Player == playerName.ToLowerInvariant());
                var playerRecord = getPlayerResult.StatusCode == OperationStatusCode.Success
                    ? getPlayerResult.Result.First()
                    : new PlayerDataRecord(playerName);
                await playerRecordsUpdater.UpdateRecord(playerRecord, matchDataRecord);
                var updatePlayerRecordResult = await playerRecordsStorage.WriteRecord(playerRecord);
            }

            await matchInfoStorage.WriteRecord(matchDataRecord);

            return new HandleResult(HttpStatusCode.OK);
        }
    }
}
