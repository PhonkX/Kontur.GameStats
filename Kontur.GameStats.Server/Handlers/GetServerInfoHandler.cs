﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
    public class GetServerInfoHandler: IHandler
    {
        private readonly IRecordsStorage<ServerDataRecord> serverInfoStorage;

        public GetServerInfoHandler(IRecordsStorage<ServerDataRecord> serverInfoStorage)
        {
            this.serverInfoStorage = serverInfoStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            var endpoint = httpServerContext.Parameters["endpoint"];
            if (String.IsNullOrWhiteSpace(endpoint))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }

            var getServerInfoResult = await serverInfoStorage.SearchRecords(x => x.Endpoint == endpoint);

            if (getServerInfoResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.NotFound);
            }

            return new HandleResult(HttpStatusCode.OK, new ServerInfo(getServerInfoResult.Result.First()));
        }
    }
}
