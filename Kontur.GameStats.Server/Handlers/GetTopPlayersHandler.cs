﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Reports;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
   public class GetTopPlayersHandler: IHandler
    {
        private readonly IRecordsStorage<PlayerDataRecord> playerRecordsStorage;

        public GetTopPlayersHandler(IRecordsStorage<PlayerDataRecord> playerRecordsStorage)
        {
            this.playerRecordsStorage = playerRecordsStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            int count = GlobalConstants.DefaultCountForReports;
            string countParameter = httpServerContext.Parameters["count"];
            if (countParameter != null)
            {
                count = Math.Min(int.Parse(countParameter), GlobalConstants.MaxCountForReports);
            } 

            if (count == 0)
            {
                return new HandleResult(HttpStatusCode.OK, new TopPlayersReport[] {});
            }

            var getTopPlayersResult = await playerRecordsStorage.GetTopNRecords(count, x => x.TotalDeaths != 0 && x.TotalMatchesPlayed > 10);

            if (getTopPlayersResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.OK, new TopPlayersReport[] {});
            }

            return new HandleResult(
                HttpStatusCode.OK,
                getTopPlayersResult.Result
                .Select(x => new TopPlayersReport(x))
                .ToArray());
        }
    }
}
