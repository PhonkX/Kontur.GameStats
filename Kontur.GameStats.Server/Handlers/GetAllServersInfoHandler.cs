﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
    public class GetAllServersInfoHandler: IHandler
    {
        private readonly IRecordsStorage<ServerDataRecord> serverInfoStorage;

        public GetAllServersInfoHandler(IRecordsStorage<ServerDataRecord> serverInfoStorage)
        {
            this.serverInfoStorage = serverInfoStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            var searchAllServersDataRecordsResult = await serverInfoStorage.SearchRecords();

            if (searchAllServersDataRecordsResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.OK, new ExtendedServerInfo[] {});
            }

            return new HandleResult(
                HttpStatusCode.OK,
                searchAllServersDataRecordsResult.Result
                .Select(x => new ExtendedServerInfo(x))
                .ToArray());
        }
    }
}
