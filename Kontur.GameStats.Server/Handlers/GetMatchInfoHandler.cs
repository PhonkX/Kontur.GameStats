﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
    public class GetMatchInfoHandler: IHandler
    {
        private readonly IRecordsStorage<ServerDataRecord> serverInfoStorage;
        private readonly IRecordsStorage<MatchDataRecord> matchInfoStorage;

        public GetMatchInfoHandler(IRecordsStorage<ServerDataRecord> serverInfoStorage, IRecordsStorage<MatchDataRecord> matchInfoStorage)
        {
            this.serverInfoStorage = serverInfoStorage;
            this.matchInfoStorage = matchInfoStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            var endpoint = httpServerContext.Parameters["endpoint"];
            if (String.IsNullOrWhiteSpace(endpoint))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }
            var getServerInfoResult = await serverInfoStorage.SearchRecords(x => x.Endpoint == endpoint);
            if (getServerInfoResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.NotFound);
            }

            var timestamp = httpServerContext.Parameters["timestamp"];
            if (String.IsNullOrWhiteSpace(timestamp))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }

            var getMatchInfoResult = await matchInfoStorage
                .SearchRecords(x => x.Server == endpoint && x.TimeStampString == timestamp);

            if (getMatchInfoResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.NotFound);
            }

            return new HandleResult(HttpStatusCode.OK, new MatchInfo(getMatchInfoResult.Result.First()));
        }
    }
}
