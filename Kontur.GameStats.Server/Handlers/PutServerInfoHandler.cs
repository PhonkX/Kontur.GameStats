﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;
using Kontur.GameStats.Server.Utilities.Json;

namespace Kontur.GameStats.Server.Handlers
{
    public class PutServerInfoHandler: IHandler
    {
        private readonly IRecordsStorage<ServerDataRecord> serverInfoStorage;
        private readonly IJsonHelper jsonHelper;

        public PutServerInfoHandler(IRecordsStorage<ServerDataRecord> serverInfoStorage, IJsonHelper jsonHelper)
        {
            this.serverInfoStorage = serverInfoStorage;
            this.jsonHelper = jsonHelper;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            var endpoint = httpServerContext.Parameters["endpoint"];
            if (String.IsNullOrWhiteSpace(endpoint))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }

            var jsonString = jsonHelper.ReadJsonString(httpServerContext.Request.InputStream);
            var serverInfo = jsonHelper.DeserializeObject<ServerInfo>(jsonString);

            ServerDataRecord serverRecord;

            var getServerRecordResult = await serverInfoStorage.SearchRecords(x => x.Endpoint == endpoint);
            if (getServerRecordResult.StatusCode == OperationStatusCode.NotFound)
            {
                serverRecord = new ServerDataRecord(endpoint, serverInfo);
            }
            else
            {
                serverRecord = getServerRecordResult.Result.First();
                serverRecord.Name = serverInfo.Name;
                serverRecord.AvailableGameModes = String.Join(",", serverInfo.GameModes);
            }

            await serverInfoStorage.WriteRecord(serverRecord);
            return new HandleResult(HttpStatusCode.OK);
        }
    }
}
