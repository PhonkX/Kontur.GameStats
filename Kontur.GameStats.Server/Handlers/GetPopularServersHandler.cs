﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Reports;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
    public class GetPopularServersHandler: IHandler
    {
        private readonly IRecordsStorage<ServerDataRecord> serverRecordsStorage;

        public GetPopularServersHandler(IRecordsStorage<ServerDataRecord> serverRecordsStorage)
        {
            this.serverRecordsStorage = serverRecordsStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            int count = GlobalConstants.DefaultCountForReports;
            string countParameter = httpServerContext.Parameters["count"];
            if (countParameter != null)
            {
                count = Math.Min(int.Parse(countParameter), GlobalConstants.MaxCountForReports);
            }

            if (count == 0)
            {
                return new HandleResult(HttpStatusCode.OK, new PopularServersReport[] {});
            }

            var getPopularServersResult = await serverRecordsStorage.GetTopNRecords(count);

            if (getPopularServersResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.OK, new PopularServersReport[] {});
            }

            return new HandleResult(
                HttpStatusCode.OK,
                getPopularServersResult.Result
                .Select(x => new PopularServersReport(x))
                .ToArray());
        }
    }
}
