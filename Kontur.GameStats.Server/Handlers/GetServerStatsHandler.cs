﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Stats;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
    public class GetServerStatsHandler: IHandler
    {
        private readonly IRecordsStorage<ServerDataRecord> serverRecordsStorage;

        public GetServerStatsHandler(IRecordsStorage<ServerDataRecord> serverRecordsStorage)
        {
            this.serverRecordsStorage = serverRecordsStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            var endpoint = httpServerContext.Parameters["endpoint"];
            if (String.IsNullOrWhiteSpace(endpoint))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }

            var getServerRecordResult = await serverRecordsStorage.SearchRecords(x => x.Endpoint == endpoint);

            if (getServerRecordResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.NotFound);
            }

            var serverStats = new ServerStats(getServerRecordResult.Result.First());
            
            return new HandleResult(HttpStatusCode.OK, serverStats);
        }
    }
}
