﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Stats;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Server.Storages;

namespace Kontur.GameStats.Server.Handlers
{
    public class GetPlayerStatsHandler: IHandler
    {
        private readonly IRecordsStorage<PlayerDataRecord> playerRecordsStorage;

        public GetPlayerStatsHandler(IRecordsStorage<PlayerDataRecord> playerRecordsStorage)
        {
            this.playerRecordsStorage = playerRecordsStorage;
        }

        public async Task<HandleResult> HandleRequestAsync(HttpServerContext httpServerContext)
        {
            var playerName = httpServerContext.Parameters["name"];
            if (String.IsNullOrWhiteSpace(playerName))
            {
                return new HandleResult(HttpStatusCode.BadRequest);
            }

            var getPlayerRecordResult = 
                await playerRecordsStorage.SearchRecords(x => x.Player == playerName.ToLowerInvariant());
            
            if (getPlayerRecordResult.StatusCode == OperationStatusCode.NotFound)
            {
                return new HandleResult(HttpStatusCode.NotFound);
            }

            var stats = new PlayerStats(getPlayerRecordResult.Result.First());

            return new HandleResult(HttpStatusCode.OK, stats);
        }
    }
}
