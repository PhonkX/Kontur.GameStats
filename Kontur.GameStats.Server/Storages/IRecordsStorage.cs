﻿using Kontur.GameStats.Server.OperationResult;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kontur.GameStats.Server.Storages
{
    public interface IRecordsStorage<T>
    {
        Task<OperationResult<IEnumerable<T>>> SearchRecords(Func<T, bool> filter = null);
        Task<OperationResult<IEnumerable<T>>> GetTopNRecords(int count, Func<T, bool> filter = null);
        Task<OperationStatusCode> WriteRecord(T record);
    }
}
