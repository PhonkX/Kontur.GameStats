﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Sql;

namespace Kontur.GameStats.Server.Storages
{
    public class ServerRecordsStorage: IRecordsStorage<ServerDataRecord>
    {
        private ServerDbContext dbContext;

        public ServerRecordsStorage()
        {
            dbContext = new ServerDbContext();
        }

        public Task<OperationResult<IEnumerable<ServerDataRecord>>> SearchRecords(Func<ServerDataRecord, bool> filter = null)
        {
            return Task.Run(() =>
            {
                var records = filter == null 
                ? dbContext.ServerDataRecords
                : dbContext.ServerDataRecords.Where(filter);

                return !records.Any()
                    ? OperationResult<IEnumerable<ServerDataRecord>>.CreateError(OperationStatusCode.NotFound)
                    : OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(records);
            });
        }

        public Task<OperationResult<IEnumerable<ServerDataRecord>>> GetTopNRecords(int count, Func<ServerDataRecord, bool> filter = null)
        {
            return Task.Run(() =>
            {
                var records = filter == null
                    ? dbContext.ServerDataRecords
                    : dbContext.ServerDataRecords.Where(filter);

                return OperationResult<IEnumerable<ServerDataRecord>>.CreateSuccessful(
                    records
                        .OrderByDescending(x => x.AverageMatchesPerDay)
                        .Take(count));
            });
        }

        public Task<OperationStatusCode> WriteRecord(ServerDataRecord record)
        {
            return Task.Run(() =>
            {
                dbContext.ServerDataRecords.AddOrUpdate(record);
                dbContext.SaveChanges();

                return OperationStatusCode.Success;
            });
        }
    }
}
