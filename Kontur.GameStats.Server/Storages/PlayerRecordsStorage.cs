﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Sql;

namespace Kontur.GameStats.Server.Storages
{
    public class PlayerRecordsStorage: IRecordsStorage<PlayerDataRecord>
    {
        private ServerDbContext dbContext;

        public PlayerRecordsStorage()
        {
            dbContext = new ServerDbContext();
        }

        public Task<OperationResult<IEnumerable<PlayerDataRecord>>> SearchRecords(Func<PlayerDataRecord, bool> filter = null)
        {
            return Task.Run(() =>
            {
                var records = filter == null
                    ? dbContext.PlayerDataRecords
                    : dbContext.PlayerDataRecords.Where(filter);

                if (!records.Any())
                {
                    return OperationResult<IEnumerable<PlayerDataRecord>>.CreateError(OperationStatusCode.NotFound);
                }
                return OperationResult<IEnumerable<PlayerDataRecord>>.CreateSuccessful(records);
            });
        }

        public Task<OperationResult<IEnumerable<PlayerDataRecord>>> GetTopNRecords(int count, Func<PlayerDataRecord, bool> filter = null)
        {
            return Task.Run(() =>
            {
                var playerDataRecords = filter == null
                    ? dbContext.PlayerDataRecords.Where(filter)
                    : dbContext.PlayerDataRecords;

                    return OperationResult<IEnumerable<PlayerDataRecord>>.CreateSuccessful(
                        playerDataRecords
                        .OrderByDescending(x => x.KillToDeathRatio)
                        .Take(count));
                });
        }

        public Task<OperationStatusCode> WriteRecord(PlayerDataRecord record)
        {
            return Task.Run(() =>
            {
                dbContext.PlayerDataRecords.AddOrUpdate(record);
                dbContext.SaveChanges();

                return OperationStatusCode.Success;
            });
        }
    }
}
