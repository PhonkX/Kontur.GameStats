﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.Server.OperationResult;
using Kontur.GameStats.Sql;

namespace Kontur.GameStats.Server.Storages
{
    public class MatchRecordsStorage: IRecordsStorage<MatchDataRecord>
    {
        private ServerDbContext dbContext;

        public MatchRecordsStorage()
        {
            dbContext = new ServerDbContext();
        }

        public Task<OperationResult<IEnumerable<MatchDataRecord>>> SearchRecords(Func<MatchDataRecord, bool> filter = null)
        {
            return Task.Run(() =>
            {
                var records = filter == null
                    ? dbContext.MatchDataRecords
                    : dbContext.MatchDataRecords.Where(filter);

                if (!records.Any())
                {
                    return OperationResult<IEnumerable<MatchDataRecord>>.CreateError(OperationStatusCode.NotFound);
                }
                return OperationResult<IEnumerable<MatchDataRecord>>.CreateSuccessful(records);
            });
        }

        public Task<OperationResult<IEnumerable<MatchDataRecord>>> GetTopNRecords(int count, Func<MatchDataRecord, bool> filter = null)
        {
            return Task.Run(() =>
            {
                var records = filter == null
                    ? dbContext.MatchDataRecords
                    : dbContext.MatchDataRecords.Where(filter);

                return OperationResult<IEnumerable<MatchDataRecord>>.CreateSuccessful(
                    records
                        .OrderByDescending(x => x.TimeStamp)
                        .Take(count)
                        .ToArray());
            });
        }

        public Task<OperationStatusCode> WriteRecord(MatchDataRecord record)
        {
            return Task.Run(() =>
            {
                dbContext.MatchDataRecords.AddOrUpdate(record);
                dbContext.SaveChanges();

                return OperationStatusCode.Success;
            });
        }
    }
}
