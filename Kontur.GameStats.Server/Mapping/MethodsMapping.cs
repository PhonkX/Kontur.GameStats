﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Kontur.GameStats.DataModel;
using Kontur.GameStats.Server.Handlers;

namespace Kontur.GameStats.Server.Mapping
{
    public class MethodsMapping
    {
        private Dictionary<UriTemplate, Type> putMethodMapping;
        private Dictionary<UriTemplate, Type> getMethodMapping;
        private string prefix;

        public MethodsMapping(string prefix)
        {
            this.prefix = prefix;
            putMethodMapping = new Dictionary<UriTemplate, Type>
            {
                [new UriTemplate("/servers/{endpoint}/info")] = typeof(PutServerInfoHandler),
                [new UriTemplate("/servers/{endpoint}/matches/{timestamp}")] = typeof(PutMatchInfoHandler),

                [new UriTemplate("/servers/{endpoint}/info/")] = typeof(PutServerInfoHandler),
                [new UriTemplate("/servers/{endpoint}/matches/{timestamp}/")] = typeof(PutMatchInfoHandler)
            };

            getMethodMapping = new Dictionary<UriTemplate, Type>
            {
                [new UriTemplate("/servers/{endpoint}/info")] = typeof(GetServerInfoHandler),
                [new UriTemplate("/servers/{endpoint}/matches/{timestamp}")] = typeof(GetMatchInfoHandler),
                [new UriTemplate("/servers/info")] = typeof(GetAllServersInfoHandler),
                [new UriTemplate("/servers/{endpoint}/stats")] = typeof(GetServerStatsHandler),
                [new UriTemplate("/players/{name}/stats")] = typeof(GetPlayerStatsHandler),
                [new UriTemplate("/reports/recent-matches/{count}")] = typeof(GetRecentMatchesHandler),
                [new UriTemplate("/reports/recent-matches")] = typeof(GetRecentMatchesHandler),
                [new UriTemplate("/reports/best-players/{count}")] = typeof(GetTopPlayersHandler),
                [new UriTemplate("/reports/best-players")] = typeof(GetTopPlayersHandler),
                [new UriTemplate("/reports/popular-servers/{count}")] = typeof(GetPopularServersHandler),
                [new UriTemplate("/reports/popular-servers")] = typeof(GetPopularServersHandler),

                [new UriTemplate("/servers/{endpoint}/info/")] = typeof(GetServerInfoHandler),
                [new UriTemplate("/servers/{endpoint}/matches/{timestamp}/")] = typeof(GetMatchInfoHandler),
                [new UriTemplate("/servers/info/")] = typeof(GetAllServersInfoHandler),
                [new UriTemplate("/servers/{endpoint}/stats/")] = typeof(GetServerStatsHandler),
                [new UriTemplate("/players/{name}/stats/")] = typeof(GetPlayerStatsHandler),
                [new UriTemplate("/reports/recent-matches/{count}/")] = typeof(GetRecentMatchesHandler),
                [new UriTemplate("/reports/recent-matches/")] = typeof(GetRecentMatchesHandler),
                [new UriTemplate("/reports/best-players/{count}/")] = typeof(GetTopPlayersHandler),
                [new UriTemplate("/reports/best-players/")] = typeof(GetTopPlayersHandler),
                [new UriTemplate("/reports/popular-servers/{count}/")] = typeof(GetPopularServersHandler),   
                [new UriTemplate("/reports/popular-servers/")] = typeof(GetPopularServersHandler) 
            };
        }

        public TemplateMatchResult GetTemplateByUrl(string httpMethod, string url)
        {
            Dictionary<UriTemplate, Type> dict = null;
            switch (httpMethod)
            {
                case "PUT":
                    dict = putMethodMapping;
                    break;
                case "GET":
                    dict = getMethodMapping;
                    break;
            }

            return GetTemplateAndParameters(dict, url);
        }

        public Type GetHandlerByTemplate(UriTemplate template, string httpMethod)
        {
            Type handler;
            switch (httpMethod)
            {
                case "PUT": handler = putMethodMapping[template];
                    break;
                case "GET":
                    handler = getMethodMapping[template];
                    break;
                default:
                    handler = null;
                    break;
            }

            return handler;
        }

        private TemplateMatchResult GetTemplateAndParameters(Dictionary<UriTemplate, Type> dict, string url)
        {
            if (dict != null)
            {
                foreach (var template in dict.Keys)
                {
                    var matchResult = template.Match(new Uri(prefix), new Uri(url));
                    if (matchResult != null)
                    {
                        var parameters = new NameValueCollection();
                        foreach (var boundVariable in matchResult.BoundVariables.AllKeys)
                        {
                            parameters[boundVariable] = matchResult.BoundVariables[boundVariable];
                        }

                        return new TemplateMatchResult
                        {
                            Template = template,
                            Parameters = parameters
                        };
                    }
                }
            }

            return null;
        }
    }
}