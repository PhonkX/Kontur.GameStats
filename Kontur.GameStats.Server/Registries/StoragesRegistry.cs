﻿using Kontur.GameStats.Server.Storages;
using SimpleInjector;

namespace Kontur.GameStats.Server.Registries
{
    public class StoragesRegistry: IRegistry
    {
        public void ApplyTo(Container container)
        {
            container.Register(typeof(IRecordsStorage<>), new[] { typeof(IRecordsStorage<>).Assembly });
        }
    }
}
