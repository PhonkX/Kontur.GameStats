﻿using SimpleInjector;

namespace Kontur.GameStats.Server.Registries
{
    public interface IRegistry
    {
        void ApplyTo(Container container);
    }
}
