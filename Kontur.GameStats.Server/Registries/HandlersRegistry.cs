﻿using Kontur.GameStats.Server.Handlers;
using SimpleInjector;

namespace Kontur.GameStats.Server.Registries
{
    public class HandlersRegistry: IRegistry
    {
        public void ApplyTo(Container container)
        {
            var assemblies = new[] { typeof(IHandler).Assembly };
            container.RegisterCollection(typeof(IHandler), assemblies);
        }
    }
}
