﻿using Kontur.GameStats.Logger;
using Kontur.GameStats.Server.Utilities.EntitiesUtilities;
using Kontur.GameStats.Server.Utilities.Json;
using SimpleInjector;

namespace Kontur.GameStats.Server.Registries
{
    public class UtilitiesRegistry: IRegistry
    {
        public void ApplyTo(Container container)
        {
            container.Register(typeof(IRecordsUpdater<>), new[] { typeof(IRecordsUpdater<>).Assembly });
            container.Register<IServerLogger, ServerLogger>(Lifestyle.Singleton);
            container.Register<IJsonHelper, JsonHelper>(Lifestyle.Singleton);
        }
    }
}
