﻿using System.Collections.Generic;
using SimpleInjector;

namespace Kontur.GameStats.Server.Registries
{
    public class RegistryManager
    {
        private readonly List<IRegistry> registries;

        public RegistryManager()
        {
            registries = new List<IRegistry> {
                new HandlersRegistry(),
                new StoragesRegistry(),
                new UtilitiesRegistry()
            };
        }

        public void ApplyRegistries(Container container)
        {
            foreach (var registry in registries)
            {
                registry.ApplyTo(container);
            }
        }
    }
}
