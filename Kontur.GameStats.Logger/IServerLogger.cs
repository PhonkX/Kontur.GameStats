﻿using log4net;

namespace Kontur.GameStats.Logger
{
    public interface IServerLogger
    {
        ILog Log { get; }
    }
}