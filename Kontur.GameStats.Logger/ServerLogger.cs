﻿using log4net;
using log4net.Config;

namespace Kontur.GameStats.Logger
{
    public class ServerLogger : IServerLogger
    {
        public ILog Log { get; }

        public ServerLogger()
        {
            Log = LogManager.GetLogger("LOGGER");
            XmlConfigurator.Configure();
        }
    }
}
