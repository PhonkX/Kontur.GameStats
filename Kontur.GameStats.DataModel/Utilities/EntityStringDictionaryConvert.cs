﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kontur.GameStats.DataModel.Utilities
{
    public static class EntityStringDictionaryConvert
    {
        public static Dictionary<string, int> ToDictionary(string entity)
        {
            var dict = new Dictionary<string, int>();
            if (!String.IsNullOrWhiteSpace(entity))
            {
                dict = entity.Split(',')
                    .Select(x => x.Split(':'))
                    .ToDictionary(x => x[0], x => int.Parse(x[1]));
            }

            return dict;
        }

        public static string ToEntityString(Dictionary<string, int> dict)
        {
            return String.Join(",",
                dict.Select(x => String.Join(":", x.Key, x.Value)));
        }
    }
}
