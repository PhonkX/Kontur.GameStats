﻿using System;
using System.Linq;
using Kontur.GameStats.DataModel.Info;

namespace Kontur.GameStats.DataModel.Utilities
{
    public static class ScoreboardStringConvertor
    {
        public static string ToString(PlayerMatchInfo[] scoreboard)
        {
            return String.Join(",", 
            scoreboard.Select(x => String.Join(":", x.Name, x.Frags, x.Kills, x.Deaths)));
        }

        public static PlayerMatchInfo[] ToScoreboard(string infoString)
        {
            return infoString
                .Split(',')
                .Select(x => new PlayerMatchInfo(x))
                .ToArray();
        }
    }
}
