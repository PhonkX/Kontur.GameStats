﻿using System.Collections.Specialized;
using System.Net;

namespace Kontur.GameStats.DataModel
{
    public class HttpServerContext
    {
        public HttpListenerRequest Request { get; set; }
        public NameValueCollection Parameters { get; set; }

        public HttpServerContext()
        {
            Parameters = new NameValueCollection();
        }
    }
}