﻿using System.ComponentModel.DataAnnotations;

namespace Kontur.GameStats.DataModel.DataRecords
{
    public class PlayerDataRecord
    {
        [Key]
        public string Player { get; set; }
        public string VisitedServers { get; set; }
        public double KillToDeathRatio { get; set; }
        public int TotalMatchesPlayed { get; set; }
        public int TotalMatchesWon { get; set; }
        public string GameModes { get; set; }
        public double AverageScoreboardPercent { get; set; }
        public int MaximumMatchesPerDay { get; set; }
        public int DaysPlayed { get; set; }
        public long LastMatchPlayed { get; set; }
        public string LastMatchPlayedTimestamp { get; set; }
        public long TotalKills { get; set; }
        public long TotalDeaths { get; set; }

        public PlayerDataRecord(string playerName)
        {
            Player = playerName.ToLowerInvariant();
            VisitedServers = "";
            GameModes = "";
            KillToDeathRatio = 0;
            TotalMatchesPlayed = 0;
            TotalMatchesWon = 0;
            TotalKills = 0;
            TotalDeaths = 0;
            LastMatchPlayed = 0;
            LastMatchPlayedTimestamp = "";
            MaximumMatchesPerDay = 0;
            AverageScoreboardPercent = 0;
            DaysPlayed = 0;
        }

        public PlayerDataRecord()
        {
            VisitedServers = "";
            GameModes = "";
        }
    }
}
