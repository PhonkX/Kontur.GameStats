﻿using System;
using System.ComponentModel.DataAnnotations;
using Kontur.GameStats.DataModel.Info;
using Kontur.GameStats.DataModel.Utilities;

namespace Kontur.GameStats.DataModel.DataRecords
{
    public class MatchDataRecord
    {
        [Key]
        public long TimeStamp { get; set; }
        public string TimeStampString { get; set; }
        public string Server { get; set; }
        public string Map { get; set; }
        public string GameMode { get; set; }
        public int FragLimit { get; set; }
        public int TimeLimit { get; set; }
        public double TimeElapsed { get; set; }
        public string Scoreboard { get; set; }

        public MatchDataRecord(string server, string timestamp, MatchInfo matchInfo)
        {
            Server = server;
            Map = matchInfo.Map;
            GameMode = matchInfo.GameMode;
            FragLimit = matchInfo.FragLimit;
            TimeLimit = matchInfo.TimeLimit;
            TimeElapsed = matchInfo.TimeElapsed;
            Scoreboard = ScoreboardStringConvertor.ToString(matchInfo.Scoreboard);
            TimeStamp = DateTime.Parse(timestamp).Ticks;
            TimeStampString = timestamp;
        }

        public MatchDataRecord()
        {
        }
    }
}
