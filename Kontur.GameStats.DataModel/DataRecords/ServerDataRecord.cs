﻿using System;
using System.ComponentModel.DataAnnotations;
using Kontur.GameStats.DataModel.Info;

namespace Kontur.GameStats.DataModel.DataRecords
{
    public class ServerDataRecord
    {
        [Key]
        public string Endpoint { get; set; }
        public string Name { get; set; }
        public string AvailableGameModes { get; set; }
        public long TotalMatchesCount { get; set; }
        public int MaxMatchesPerDayCount { get; set; }
        public double AverageMatchesPerDay { get; set; }
        public int UpTimeInDays { get; set; }
        public long TotalPlayersCount { get; set; }
        public int MaxPopulation { get; set; }
        public double AveragePopulation { get; set; }
        public long LastMatchPlayedTimeStamp { get; set; }
        public string PlayedGameModes { get; set; }
        public string Maps { get; set; }

        public ServerDataRecord(string endpoint, ServerInfo info)
        {
            Endpoint = endpoint;
            Name = info.Name;
            //AvailableGameModes = new string[info.GameModes.Length];
            AvailableGameModes = String.Join(",", info.GameModes);
            TotalMatchesCount = 0;
            TotalPlayersCount = 0;
            MaxMatchesPerDayCount = 0;
            AverageMatchesPerDay = 0;
            UpTimeInDays = 0;
            MaxPopulation = 0;
            AveragePopulation = 0;
            PlayedGameModes = "";
            Maps = "";
        }

        public ServerDataRecord()
        {
            PlayedGameModes = "";
            Maps = "";
            TotalMatchesCount = 0;
            TotalPlayersCount = 0;
            MaxMatchesPerDayCount = 0;
            AverageMatchesPerDay = 0;
            UpTimeInDays = 0;
            MaxPopulation = 0;
            AveragePopulation = 0;
        }
    }
}
