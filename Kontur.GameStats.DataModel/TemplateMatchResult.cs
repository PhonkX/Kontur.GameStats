﻿using System;
using System.Collections.Specialized;

namespace Kontur.GameStats.DataModel
{
    public class TemplateMatchResult
    {
        public UriTemplate Template;
        public NameValueCollection Parameters;
    }
}
