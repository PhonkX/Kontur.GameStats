﻿using System.Linq;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Utilities;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Stats
{
    public class PlayerStats
    {
        [JsonProperty("totalMatchesPlayed")]
        public int TotalMatchesPlayed;
        [JsonProperty("totalMatchesWon")]
        public int TotalMatchesWon;
        [JsonProperty("favoriteServer")]
        public string FavoriteServer;
        [JsonProperty("uniqueServers")]
        public int UniqueServers;
        [JsonProperty("favoriteGameMode")]
        public string FavoriteGameMode;
        [JsonProperty("averageScoreboardPercent")]
        public double AverageScoreboardPercent;
        [JsonProperty("maximumMatchesPerDay")]
        public int MaximumMatchesPerDay;
        [JsonProperty("averageMatchesPerDay")]
        public double AverageMatchesPerDay;
        [JsonProperty("lastMatchPlayed;")]
        public string LastMatchPlayed;
        [JsonProperty("killToDeathRatio")]
        public double KillToDeathRatio;

        public PlayerStats(PlayerDataRecord playerDataRecord)
        {
            TotalMatchesPlayed = playerDataRecord.TotalMatchesPlayed;
            TotalMatchesWon = playerDataRecord.TotalMatchesWon;

            var visitedServers = EntityStringDictionaryConvert.ToDictionary(playerDataRecord.VisitedServers);
            FavoriteServer = visitedServers
                .OrderBy(x => x.Value)
                .First()
                .Key;
            UniqueServers = visitedServers.Count;

            FavoriteGameMode = EntityStringDictionaryConvert.ToDictionary(playerDataRecord.GameModes)
                .OrderBy(x => x.Value)
                .First()
                .Key;

            AverageScoreboardPercent = playerDataRecord.AverageScoreboardPercent;
            MaximumMatchesPerDay = playerDataRecord.MaximumMatchesPerDay;
            AverageMatchesPerDay = playerDataRecord.DaysPlayed != 0?
                playerDataRecord.TotalMatchesPlayed/playerDataRecord.DaysPlayed
                : 0;
            LastMatchPlayed = playerDataRecord.LastMatchPlayedTimestamp;
            KillToDeathRatio = playerDataRecord.KillToDeathRatio;
        }
    }
}
