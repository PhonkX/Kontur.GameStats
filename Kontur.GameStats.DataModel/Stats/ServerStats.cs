﻿using System.Linq;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Utilities;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Stats
{
    public class ServerStats
    {
        [JsonProperty("totalMatchesPlayed")]
        public long TotalMatchesPlayed;
        [JsonProperty("maximumMatchesPerDay")]
        public int MaximumMatchesPerDay;
        [JsonProperty("averageMathesPerDay")]
        public double AverageMathesPerDay;
        [JsonProperty("maximumPopulation")]
        public int MaximumPopulation;
        [JsonProperty("averagePopulation")]
        public double AveragePopulation;
        [JsonProperty("top5GameModes")]
        public string[] Top5GameModes;
        [JsonProperty("top5Maps")]
        public string[] Top5Maps;

        public ServerStats(ServerDataRecord serverDataRecord)
        {
            TotalMatchesPlayed = serverDataRecord.TotalMatchesCount;
            MaximumMatchesPerDay = serverDataRecord.MaxMatchesPerDayCount;
            AveragePopulation = serverDataRecord.TotalMatchesCount != 0
                ? serverDataRecord.TotalPlayersCount/serverDataRecord.TotalMatchesCount
                : 0;
            AverageMathesPerDay = serverDataRecord.UpTimeInDays != 0 ?
                serverDataRecord.TotalMatchesCount/serverDataRecord.UpTimeInDays
                : 0;
            MaximumPopulation = serverDataRecord.MaxPopulation;

            Top5GameModes = EntityStringDictionaryConvert.ToDictionary(serverDataRecord.PlayedGameModes)
                .OrderBy(x => x.Value)
                .Take(GlobalConstants.GameInfoCountForTops)
                .Select(x => x.Key)
                .ToArray();

            Top5Maps = EntityStringDictionaryConvert.ToDictionary(serverDataRecord.Maps)
                .OrderBy(x => x.Value)
                .Take(GlobalConstants.GameInfoCountForTops)
                .Select(x => x.Key)
                .ToArray();
        }
    }
}
