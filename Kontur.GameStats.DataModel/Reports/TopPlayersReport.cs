﻿using Kontur.GameStats.DataModel.DataRecords;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Reports
{
    public class TopPlayersReport
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("killToDeathRatio")]
        public double KillToDeathRatio;

        public TopPlayersReport(PlayerDataRecord playerDataRecord)
        {
            Name = playerDataRecord.Player;
            KillToDeathRatio = playerDataRecord.KillToDeathRatio;
        }
    }
}
