﻿using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Reports
{
    public class RecentMatchesReport
    {
        [JsonProperty("server")]
        public string Server;
        [JsonProperty("timestamp")]
        public string Timestamp;
        [JsonProperty("results")]
        public MatchInfo Results;

        public RecentMatchesReport(MatchDataRecord matchDataRecord)
        {
            Server = matchDataRecord.Server;
            Timestamp = matchDataRecord.TimeStampString;
            Results = new MatchInfo(matchDataRecord);
        }
    }
}
