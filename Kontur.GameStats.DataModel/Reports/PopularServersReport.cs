﻿using Kontur.GameStats.DataModel.DataRecords;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Reports
{
    public class PopularServersReport
    {
        [JsonProperty("endpoint")]
        public string Endpoint;
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("averageMatchesPerDay")]
        public double AverageMatchesPerDay;

        public PopularServersReport(ServerDataRecord serverDataRecord)
        {
            Endpoint = serverDataRecord.Endpoint;
            Name = serverDataRecord.Name;
            AverageMatchesPerDay = serverDataRecord.AverageMatchesPerDay;
        }
    }
}
