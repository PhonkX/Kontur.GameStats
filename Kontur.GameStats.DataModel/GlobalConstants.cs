﻿namespace Kontur.GameStats.DataModel
{
    public static class GlobalConstants
    {
        public const int DefaultCountForReports = 5;
        public const int MaxCountForReports = 50;
        public const int GameInfoCountForTops = 5;
    }
}
