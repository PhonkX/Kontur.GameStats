﻿using Kontur.GameStats.DataModel.DataRecords;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Info
{
    public class ExtendedServerInfo
    {
        [JsonProperty("endpoint")]
        public string Endpoint;
        [JsonProperty("info")]
        public ServerInfo Info;

        public ExtendedServerInfo(ServerDataRecord serverDataRecord)
        {
            Endpoint = serverDataRecord.Endpoint;
            Info = new ServerInfo(serverDataRecord);
        }
    }
}
