﻿using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Utilities;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Info
{
    public class MatchInfo
    {
        [JsonProperty("map")]
        public string Map;
        [JsonProperty("gameMode")]
        public string GameMode;
        [JsonProperty("fragLimit")]
        public int FragLimit;
        [JsonProperty("timeLimit")]
        public int TimeLimit;
        [JsonProperty("timeElapsed")]
        public double TimeElapsed;
        [JsonProperty("scoreboard")]
        public PlayerMatchInfo[] Scoreboard;

        public MatchInfo(MatchDataRecord matchDataRecord)
        {
            Map = matchDataRecord.Map;
            GameMode = matchDataRecord.GameMode;
            FragLimit = matchDataRecord.FragLimit;
            TimeElapsed = matchDataRecord.TimeElapsed;
            TimeLimit = matchDataRecord.TimeLimit;
            Scoreboard = ScoreboardStringConvertor.ToScoreboard(matchDataRecord.Scoreboard);
        }

        public MatchInfo()
        {
        }
    }
}
