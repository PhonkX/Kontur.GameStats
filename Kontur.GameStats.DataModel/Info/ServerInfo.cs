﻿using Kontur.GameStats.DataModel.DataRecords;
using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Info
{
    public class ServerInfo
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("gameModes")]
        public string[] GameModes;

        public ServerInfo(ServerDataRecord serverDataRecord)
        {
            Name = serverDataRecord.Name;
            GameModes = serverDataRecord.AvailableGameModes.Split(',');
        }

        public ServerInfo()
        {
        }
    }
}
