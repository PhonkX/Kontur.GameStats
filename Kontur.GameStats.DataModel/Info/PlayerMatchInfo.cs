﻿using Newtonsoft.Json;

namespace Kontur.GameStats.DataModel.Info
{
    public class PlayerMatchInfo
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("frags")]
        public int Frags;
        [JsonProperty("kills")]
        public int Kills;
        [JsonProperty("deaths")]
        public int Deaths;

        public PlayerMatchInfo()
        {
        }

        public PlayerMatchInfo(string infoString)
        {
            var info = infoString.Split(':');
            Name = info[0];
            Frags = int.Parse(info[1]);
            Kills = int.Parse(info[2]);
            Deaths = int.Parse(info[3]);
        }
    }
}
