﻿using System.Net;

namespace Kontur.GameStats.DataModel
{
    public class HandleResult
    {
        public HttpStatusCode StatusCode { get; }
        public object DataTemporaryObject { get; }

        public HandleResult(HttpStatusCode code, object dto = null)
        {
            StatusCode = code;
            DataTemporaryObject = dto;
        }
    }
}
