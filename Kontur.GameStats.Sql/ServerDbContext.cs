﻿using System.Data.Entity;
using Kontur.GameStats.DataModel.DataRecords;

namespace Kontur.GameStats.Sql
{
    public class ServerDbContext: DbContext
    {
        public IDbSet<ServerDataRecord> ServerDataRecords { get; set; }
        public IDbSet<MatchDataRecord> MatchDataRecords { get; set; }
        public IDbSet<PlayerDataRecord> PlayerDataRecords { get; set; }
    }

    public class ServerDbInitializer : CreateDatabaseIfNotExists<ServerDbContext>
    {

    }
}
