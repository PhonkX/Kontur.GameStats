﻿using System.Data.Entity;
using System.Linq;
using FluentAssertions;
using Kontur.GameStats.DataModel.DataRecords;
using Kontur.GameStats.DataModel.Info;
using NUnit.Framework;

namespace Kontur.GameStats.Sql.IntegrationTests
{
    [TestFixture]
    public class IntegrationTests
    {
        private ServerDbContext dbContext;
        private ServerDataRecord testRecord;

        [OneTimeSetUp]
        public void Setup()
        {
            Database.SetInitializer(new ServerDbInitializer());
            dbContext = new ServerDbContext();
            
            var info = new ServerInfo
            {
                Name = "Practice DM",
                GameModes = new[] { "DM" }
            };
            testRecord = new ServerDataRecord("192.168.0.1-28965", info);
            //var testRecord2 = new ServerDataRecord("192.168.0.1-28964", info);
            //dbContext.ServerDataRecords.Remove(testRecord);
            dbContext.ServerDataRecords.Add(testRecord);
            //dbContext.ServerDataRecords.Add(testRecord2);
            dbContext.SaveChanges();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            dbContext.ServerDataRecords.Remove(testRecord);
        }

        [Test]
        public void ServerDataRecords_Should_ContainWrittenRecord()
        {
            var record = dbContext.ServerDataRecords.First(x => x.Endpoint == testRecord.Endpoint);
            record.ShouldBeEquivalentTo(testRecord);
        }
    }
}
